import os
import django
import sys
from django.utils import timezone

parent_directory = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
sys.path.append(parent_directory)

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'vendor_management_project.settings')

django.setup()

from vendor_management_app.models import Vendor

vendor_data = [
    {'name': 'Vendor 1', 'contact_details': 'Contact details 1', 'address': 'Address 1', 'vendor_code': 'V1'},
    {'name': 'Vendor 2', 'contact_details': 'Contact details 2', 'address': 'Address 2', 'vendor_code': 'V2'},
    {'name': 'Vendor 3', 'contact_details': 'Contact details 3', 'address': 'Address 3', 'vendor_code': 'V3'},
    {'name': 'Vendor 4', 'contact_details': 'Contact details 4', 'address': 'Address 4', 'vendor_code': 'V4'},
    {'name': 'Vendor 5', 'contact_details': 'Contact details 5', 'address': 'Address 5', 'vendor_code': 'V5'},
    {'name': 'Vendor 6', 'contact_details': 'Contact details 6', 'address': 'Address 6', 'vendor_code': 'V6'},
    {'name': 'Vendor 7', 'contact_details': 'Contact details 7', 'address': 'Address 7', 'vendor_code': 'V7'},
    {'name': 'Vendor 8', 'contact_details': 'Contact details 8', 'address': 'Address 8', 'vendor_code': 'V8'},
]

for data in vendor_data:
    Vendor.objects.create(**data)

print("Data inserted successfully.")
