# urls.py

from django.urls import path
from . import views

urlpatterns = [
    path('api/get_or_create/vendors/', views.vendor_list_create),
    path('api/get_or_alter/vendors/', views.vendor_detail),
]
